class Match< ApplicationRecord
    belongs_to :league
    belongs_to :season
    belongs_to :post_type
    has_and_belongs_to_many :teams, dependent: :destroy
    has_and_belongs_to_many :tags, dependent: :destroy
    validates :title, :datatime, :season_id, :league_id, :team_ids, :gender, :url, :post_type_id, presence: true
end   
