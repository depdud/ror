class Tag < ApplicationRecord
    has_and_belongs_to_many :matches, dependent: :destroy
    validates :title, :description, presence: true
    validates_uniqueness_of :title
end   
