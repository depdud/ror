class League < ApplicationRecord
    has_many :matches, dependent: :destroy
    validates :title, presence: true
    validates_uniqueness_of :title, :id
end 
