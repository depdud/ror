class PostType < ApplicationRecord
    has_many :matches
    validates :title, presence: true
    validates_uniqueness_of :title
end   
