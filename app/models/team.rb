class Team < ApplicationRecord
    has_and_belongs_to_many :matches, dependent: :destroy
    validates :title, presence: true
    validates_uniqueness_of :title, :id
end  
