class Application::IndexController < ApplicationController
    layout "application"
    def index
      render template: "layouts/application"
    end
end
