class UsersController < ApplicationController
  layout "admin"
  before_action :access

  def index
    @users = User.all.order(:id)
  end

  def edit
    @users = User.find(params[:id])
  end

  def update
    @users = User.find(params[:id])

    if @users.update(user_params)
      redirect_to users_path
    else
      render 'edit'
    end
  end

  def destroy
    @users = User.find(params[:id])
    @users.destroy

    redirect_to users_path
  end

  private
  def user_params
    params.require(:user).permit(:fio, :email, :is_admin)
  end
end
