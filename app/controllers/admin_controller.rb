class AdminController < ApplicationController
  layout "admin"
  before_action :access
  
  def index
    @matches = Match.order(id: :desc).first(5)
    @teams = Team.order(id: :desc).first(5)
    @tags = Tag.order(id: :desc).first(5)
    @post_types = PostType.order(id: :desc).first(5)
    @leagues = League.order(id: :desc).first(5)
    @seasons = Season.order(id: :desc).first(5)
    @weeks = Match.
        joins(:league, :season, :post_type, :tags, :teams).
        where("datatime >= NOW() AND datatime <= NOW() + INTERVAL 7 DAY").order(:datatime).uniq
    @previous = Match.
        joins(:league, :season, :post_type, :tags, :teams).
        where("datatime <= NOW() AND datatime >= NOW() - INTERVAL 7 DAY").order(:datatime).uniq
  end
end
