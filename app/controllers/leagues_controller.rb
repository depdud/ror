class LeaguesController < ApplicationController
  layout "admin"
  before_action :access

  def index
    @leagues = League.all.order(:title)
  end

  def new
    @leagues = League.new
  end

  def edit
    @leagues = League.find(params[:id])
  end

  def create
    @leagues = League.new(league_params)

    if @leagues.save
      redirect_to leagues_path
    else
      render 'new'
    end
  end

  def update
    @leagues = League.find(params[:id])

    if @leagues.update(league_params)
      redirect_to leagues_path
    else
      render 'edit'
    end
  end

  def destroy
    @leagues = League.find(params[:id])
    @leagues.destroy

    redirect_to leagues_path
  end

  private
  def league_params
    params.require(:league).permit(:title)
  end
end
