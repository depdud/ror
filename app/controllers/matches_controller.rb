class MatchesController < ApplicationController
  layout "admin"
  before_action :access

  def index
    @matches = Match.
        joins(:teams).
        order(:datatime).uniq
  end

  def show
    @matches = Match.joins(:league, :season, :post_type, :tags, :teams).find(params[:id])
  end

  def new
    @matches = Match.new
  end

  def edit
    @matches = Match.find(params[:id])
  end

  def create
    @matches = Match.new(match_params)

    if @matches.save
      redirect_to matches_path
    else
      render 'new'
    end
  end

  def update
    @matches = Match.find(params[:id])

    if @matches.update(match_params)
      redirect_to matches_path
    else
      render 'edit'
    end
  end

  def destroy
    @matches = Match.find(params[:id])
    @matches.destroy

    redirect_to matches_path
  end

  private
  def match_params
    params.require(:match).permit(:title, :datatime, :description, :gender, :tour, :competition, :url, :league_id, :season_id, :post_type_id, team_ids:[], tag_ids:[])
  end
end
