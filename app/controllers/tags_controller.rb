class TagsController < ApplicationController
  layout "admin"
  before_action :access

  def index
    @tags = Tag.all.order(:title)
  end

  def new
    @tags = Tag.new
  end

  def edit
    @tags = Tag.find(params[:id])
  end

  def create
    @tags = Tag.new(tag_params)

    if @tags.save
      redirect_to tags_path
    else
      render 'new'
    end
  end

  def update
    @tags = Tag.find(params[:id])

    if @tags.update(tag_params)
      redirect_to tags_path
    else
      render 'edit'
    end
  end

  def destroy
    @tags = Tag.find(params[:id])
    @tags.destroy

    redirect_to tags_path
  end

  private
  def tag_params
    params.require(:tag).permit(:title, :description)
  end
end
