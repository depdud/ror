class PostTypesController < ApplicationController
  layout "admin"
  before_action :access

  def index
    @post_types = PostType.all.order(:title)
  end

  def new
    @post_types = PostType.new
  end

  def edit
    @post_types = PostType.find(params[:id])
  end

  def create
    @post_types = PostType.new(post_type_params)

    if @post_types.save
      redirect_to post_types_path
    else
      render 'new'
    end
  end

  def update
    @post_types = PostType.find(params[:id])

    if @post_types.update(post_type_params)
      redirect_to post_types_path
    else
      render 'edit'
    end
  end

  def destroy
    @post_types = PostType.find(params[:id])
    @post_types.destroy

    redirect_to post_types_path
  end

  private
  def post_type_params
    params.require(:post_type).permit(:title)
  end
end
