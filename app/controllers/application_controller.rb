class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  def access
    if authenticate_user!
      unless current_user.is_admin?
        redirect_to index_path
      end
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:fio, :is_admin])
  end
end
