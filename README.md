# Подготовка
1. Установить `docker` ([Install Docker](https://docs.docker.com/engine/install/ubuntu/)) и `docker-compose` ([Install Docker-Compose](https://docs.docker.com/compose/install/))
2. Настроить доступ к `docker` без `sudo` ([Non-root User](https://docs.docker.com/engine/install/linux-postinstall/))
3. Скачать репозиторий (два способа):
    1. Архивом
    2. С помощью `git clone https://gitlab.inventos.ru/internaruta/light-webcaster-web.git`
4. Открыть терминал и перейти к папке проекта с помощью `cd path/to/project`
# Запуск
1. `docker-compose build`
3. `docker-compose run web rake db:create` (при возникновении ошибки попробовать запустить команду с использованием `sudo`)
4. `docker-compose run web rake db:migrate`
5. `docker-compose run web rake db:seed`
6. `docker-compose up`

# Импорт информации с tv.rushandball.ru
*  `rake import:leagues` - импортировать лиги
*  `rake import:seasons` - импортировать сезоны
*  `rake import` - импортировать всё (в.т.ч. матчи)

# Список примечаний
1. Пользователь с правами доступа 
    1. Логин: ad@ad.ad 
    2. Пароль: qweqwe
2. Пользователь без прав доступа
    1. Логин: qwe@qwe.qwe
    2. Пароль: qweqwe
