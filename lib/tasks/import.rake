namespace 'import' do
  desc "Спарсить лиги"
  task :leagues => :environment do 
    require 'open-uri'
    require 'nokogiri'
    url = "https://tv.rushandball.ru/ajax.php?cmd=change_videobox_tab&currenttab=0&page=1&id=470&lang=ru&menu_id=470&portal=all&_mediatype=&_sport=&_league=&_saison=&_teams=&_pool=&_competition=&_gender=&_round=&_group=&_organisation=&_stage=&_division=&_court="
    html = URI.open(url, encoding: Encoding::UTF_8)
    puts html.to_s
    doc = Nokogiri::HTML(html, nil, Encoding::UTF_8.to_s)
    puts "Добавленные лиги:"
    doc.css('.filter_select2[data-name="_league"] option').each do |option|
      #небольшая ремарка: название здесь проверяется потому, что в некоторых случаях id почему-то недостаточно ¯\_(ツ)_/¯
      unless option.text.empty? || League.exists?(id: option['value'].to_i) || League.exists?(title:option.text.strip) then
        league = League.create(title: option.text.strip, id: option['value'].to_i)
        puts league.title.to_s + " (" + league.id.to_s + ")"
      end
    end
  end
  desc "Спарсить сезоны"
  task :seasons => :environment do 
    require 'open-uri'
    require 'nokogiri'
    url = "https://tv.rushandball.ru/ajax.php?cmd=change_videobox_tab&currenttab=0&page=1&id=470&lang=ru&menu_id=470&portal=all&_mediatype=&_sport=&_league=&_saison=&_teams=&_pool=&_competition=&_gender=&_round=&_group=&_organisation=&_stage=&_division=&_court="
    html = URI.open(url, encoding: Encoding::UTF_8)
    puts html.to_s
    doc = Nokogiri::HTML(html, nil, Encoding::UTF_8.to_s)
    puts "Добавленные сезоны:"
    doc.css('.filter_select2[data-name="_saison"] option').each do |option|
      unless option.text.empty? || Season.exists?(id: option['value'].to_i) || Season.exists?(title: option.text.strip) then
        season = Season.create(title: option.text.strip, id: option['value'].to_i)
        puts season.title + " (" + season.id.to_s + ")"
      end
    end
  end

  task :default => ['environment', 'import:leagues', 'import:seasons'] do
    require 'open-uri'
    require 'nokogiri'
    doc = getPageHtml("https://tv.rushandball.ru/ajax.php?cmd=change_videobox_tab&currenttab=0&page=1&id=470&lang=ru&menu_id=470&portal=all&_mediatype=&_sport=&_league=&_saison=&_teams=&_pool=&_competition=&_gender=&_round=&_group=&_organisation=&_stage=&_division=&_court=")
    maxCount = 0
    doc.css('li.paging-max-value').each do |maxV|
      maxCount = maxV.text.to_i
    end
    puts "Количество страниц:" + maxCount.to_s
    for i in 1..maxCount do
      puts "Парсинг страницы " + i.to_s
      parsePage("https://tv.rushandball.ru/ajax.php?cmd=change_videobox_tab&currenttab=0&page=" + i.to_s + "&id=470&lang=ru&menu_id=470&portal=all&_mediatype=&_sport=&_league=&_saison=&_teams=&_pool=&_competition=&_gender=&_round=&_group=&_organisation=&_stage=&_division=&_court=")
    end
  end
end

desc "Импортировать всё, включая игры"
task import: 'import:default'

def getPageHtml(url)
  Nokogiri::HTML(URI.open(url, encoding: Encoding::UTF_8), nil, Encoding::UTF_8.to_s)
end

def parsePage(url)
  doc = getPageHtml(url)
  doc.css('.teaser a').each do |teaser|
    parseMatch(teaser)
  end
end

def parseMatch(teaser)
  page = getPageHtml('https://tv.rushandball.ru' + teaser['href'])
  info = Hash.new
  match = Match.new
  page.css('.info time').take(1).each do |time|
    match.datatime = time.text.strip.to_datetime
  end
  teaser.css('.imgbox img').take(1).each do |thumbnail|
    match.url = thumbnail['src']
  end
  importTeams(page, match)
  importData(page, match)
  importCategory(page, match)

  unless Match.exists?(title: match.title, datatime: match.datatime)
    match.save
  end
end

def importData(page, match) 
  page.css('.data .row .td').each_with_index do |td, index|
    case index
    when 0 #стрип на всякий случай. В зависимостях проставлен импорт всех лиг, команд и пр., так что здесь create не юзается, только find_by
      match.league = League.find_by(title: td.text.strip)
    when 2
      match.season = Season.find_by(title: td.text.strip)
    when 4
      match.gender = td.text.strip
    end
  end
end

def importCategory(page, match) 
  page.css('.topcontent .category').take(1).each do |category|
    categoryName = category.text.strip
    if PostType.exists?(title: categoryName) 
      match.post_type = PostType.find_by(title: categoryName)
    else
      match.post_type = PostType.create(title: categoryName)
      unless categoryName.empty?
        puts 'Новый тип поста: ' + categoryName
      end
    end
  end
end

def importTeams(page, match)
  page.css('.info h1').take(1).each do |title|
    match.title = validateMatchTitle(title.text.strip)
    teams = match.title.split(' - ')
    if teams.size > 1
      teamsArr = Array.new(2)
      teams.each_with_index do |team, index|
        if Team.exists?(title: team.strip)
          teamsArr[index] = Team.find_by(title: team.strip)
        else
          teamsArr[index] = Team.create(title: team.strip)
        end
      end
      match.teams = teamsArr
    end
  end
end

def validateMatchTitle(title)
  if title.match(/женщины|мужчины/i)
    return '' #на пустой заголовок не пройдёт валидация базы, и он не добавится
  else
    title = title.gsub(/(\d+. ?)/, '').gsub(/(")/, '')
  end
  return title
end