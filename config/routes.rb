Rails.application.routes.draw do
  #get 'parser/rushandball'
  devise_for :users
  resources :admin, only: [:index]
  scope '/admin' do
    resources :teams, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :leagues, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :seasons, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :tags, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :post_types, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :users, only: [:index, :edit, :update, :destroy]
    resources :matches, shallow: true
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/admin', to: 'admin#index', as: 'user_root'
  get '/', to: 'application/index#index', as: 'index'
end
