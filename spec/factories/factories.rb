FactoryBot.define do

  factory :user, class: User do
    email { "joe@gmail.com" }
    password { '12341234' }
    fio {'Виктор Петрович Фролов'}
    is_admin { true }
  end
end
