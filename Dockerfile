FROM ruby:2.7.0
RUN groupadd -g 1000 app \
    && useradd -g app -u 1000 -g 1000  -d /myapp -s /sbin/bash -c "Docker image user" app
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN apt-get install gcc g++ make
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  &&  echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
  &&  apt-get update && apt-get install -y yarn

RUN mkdir /myapp

WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD . /myapp
RUN yarn install --check-files
#RUN bundle exec rake assets:precompile
#RUN chown -R 1000:1000  /myapp/tmp /myapp/public
#USER app
