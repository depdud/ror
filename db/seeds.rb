# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

League.create([{ title: 'Кубок России' },
               { title: 'Сборная'},
               { title: 'IHF WBH Championship'},
               { title: 'Первенство (мол.)'},
               { title: 'Cуперлига'},
               { title: 'Суперкубок'}])

Season.create([{title: '2019/20'},
               {title: '2018/19'},
               {title: '2017/18'},
               {title: '2016/17'},
               {title: '2015/16'},
               {title: '2014/15'},
               {title: '2013/14'}])

Team.create([{title: 'Акбузат'},
             {title: 'ЦСКА'},
             {title: 'Олимп'},
             {title: 'Виктор'},
             {title: 'Ростов-Дон-3'},
             {title: 'Ростов-Дон-2-UOR'},
             {title: 'Звезда-UOR'}])

PostType.create([{title: 'Прямая трансляция'},
             {title: 'Полный матч'},
             {title: 'Нарезка'},
             {title: 'Моменты матча'},
             {title: 'Командная игра'},
             {title: 'Лучший игрок'},
             {title: 'Видеоряд'}])

Tag.create([{title: 'Один', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'},
                 {title: 'Два', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'},
                 {title: 'Три', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'},
                 {title: 'Четыре', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'},
                 {title: 'Пять', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'},
                 {title: 'Шесть', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'},
                 {title: 'Семь', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)'}])

Match.create([{title: 'Один', datatime: '2020-07-06T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Мужчины', tour: 'Финал', competition: 'Один', team_ids: %w[1 2], tag_ids: %w[1 2], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '1', season_id: '1', post_type_id: '1', },
              {title: 'Два', datatime: '2020-07-07T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Женщины', tour: 'Финал', competition: 'Два', team_ids: %w[1 3], tag_ids: %w[1 3], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '2', season_id: '2', post_type_id: '2', },
              {title: 'Три', datatime: '2020-07-08T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Мужчины', tour: 'Финал', competition: 'Три', team_ids: %w[1 4], tag_ids: %w[1 4], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '3', season_id: '3', post_type_id: '3', },
              {title: 'Четыре', datatime: '2020-07-04T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Женщины', tour: 'Финал', competition: 'Четыре', team_ids: %w[2 3], tag_ids: %w[2 3], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '4', season_id: '4', post_type_id: '4', },
              {title: 'Пять', datatime: '2020-07-03T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Мужчины', tour: 'Финал', competition: 'Пять', team_ids: %w[2 5], tag_ids: %w[1 2 3], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '5', season_id: '5', post_type_id: '5', },
              {title: 'Шесть', datatime: '2020-07-09T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Женщины', tour: 'Финал', competition: 'Шесть', team_ids: %w[3 7], tag_ids: %w[1 2 5], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '6', season_id: '6', post_type_id: '5', },
              {title: 'Семь', datatime: '2020-07-10T09:33', description: 'Lorem ipsum — классический текст-«рыба» (условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы)', gender: 'Мужчины', tour: 'Финал', competition: 'Семь', team_ids: %w[4 5], tag_ids: %w[1 2 6 7], url: 'https://www.youtube.com/watch?v=Bve_ASkIZtY', league_id: '7', season_id: '7', post_type_id: '7', }])

user = User.new
user.email = 'ad@ad.ad'
user.password = 'qweqwe'
user.encrypted_password = '$2a$12$FlUozBxoOS.eu6Y05R0dcO9NMMencm6sFYepQn/AbR.74V1A1B7OK'
user.fio = 'Администратор'
user.is_admin = '1'
user.save!

user = User.new
user.email = 'qwe@qwe.qwe'
user.password = 'qweqwe'
user.encrypted_password = '$2a$12$FlUozBxoOS.eu6Y05R0dcO9NMMencm6sFYepQn/AbR.74V1A1B7OK'
user.fio = 'Не администратор'
user.is_admin = '0'
user.save!