class InitDatabase < ActiveRecord::Migration[6.0]
  def change
      create_table :leagues do |t|
          t.string :title
      end
      create_table :seasons do |t|
          t.string :title
      end
      create_table :teams do |t|
          t.string :title
      end
      create_table :post_types do |t|
          t.string :title
      end
      create_table :tags do |t|
          t.string :title
          t.text :description
      end
      create_table :matches do |t|
          t.string :title
          t.datetime :datatime
          t.text :description
          t.string :gender
          t.string :tour
          t.string :competition
          t.string :url
          t.integer :league_id
          t.integer :season_id
          t.integer :post_type_id
      end
      # create_table :team_matches do |t|
      #     t.integer :match_id
      #     t.integer :team_id
      # end
      # create_table :users do |t|
      #   t.string :surname
      #   t.string :name
      #   t.string :login
      #   t.string :password
      #   t.string :role
      # end
    #   create_table :posts do |t|
    #     t.integer :tag_id
    #     t.integer :match_id
    #     t.integer :team_id
    #     t.integer :post_type_id
    #     t.integer :user_id
    # end
  end
end
