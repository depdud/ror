class CreateJoinTableMatchTag < ActiveRecord::Migration[6.0]
  def change
    create_join_table :matches, :tags do |t|
      t.index [:match_id, :tag_id]
      t.index [:tag_id, :match_id]
    end
  end
end
